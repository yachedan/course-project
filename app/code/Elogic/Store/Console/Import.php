<?php
declare(strict_types=1);
namespace Elogic\Store\Console;
use Elogic\Store\Service\GenericCSVImport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Import extends Command
{

    /**
     * @var GenericCSVImport[]
     */
    private $importers;
    /**
     * @var string
     */
    private $name;


    /**
     * __construct
     *
     * @param string $name
     * @param GenericCSVImport[] $importers
     * @return void
     */
    public function __construct($name = null, array $importers = [])
    {
        parent::__construct($name);
        $this->importers = $importers;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('elogic:import');
        $this->setDescription('Import store csv data.');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) : void
    {
        foreach ($this->importers as $importer) {
            $result = $importer->execute();
            if ($result) {
                $output->writeln('<info>Imported</info>');
            } else {
                $output->writeln('<info>Failed</info>');
            }
        }

    }
}
