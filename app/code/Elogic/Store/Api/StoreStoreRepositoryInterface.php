<?php
declare(strict_types=1);

namespace Elogic\Store\Api;

use Elogic\Store\Api\Data\StoreStoreInterface;
use Elogic\Store\Api\Data\StoreSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;

interface StoreStoreRepositoryInterface
{

    /**
     * @param int $id
     * @return StoreStoreInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id) : StoreStoreInterface;
    /**
     * @param int $id
     * @param int $store
     * @return StoreStoreInterface
     * @throws NoSuchEntityException
     */
    public function getByIds(int $id, int $store) : StoreStoreInterface;

    /**
     * @param StoreStoreInterface $store
     * @return void
     */
    public function save(StoreStoreInterface $store) : void;

    /**
     * @param StoreStoreInterface $store
     * @return  void
     */
    public function delete(StoreStoreInterface $store) : void;

    /**
     * @param int $id
     * @return void
     */
    public function deleteById(int $id) : void;
}
