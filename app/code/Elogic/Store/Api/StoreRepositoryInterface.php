<?php
declare(strict_types=1);

namespace Elogic\Store\Api;

use Elogic\Store\Api\Data\StoreInterface;
use Elogic\Store\Api\Data\StoreSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;

interface StoreRepositoryInterface
{

    /**
     * @param int $id
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id) : StoreInterface;

    /**
     * @param string $urlKey
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    public function getByUrlKey(string $urlKey) : StoreInterface;

    /**
     * @param StoreInterface $store
     * @return void
     */
    public function save(StoreInterface $store) : void;

    /**
     * @param StoreInterface $store
     * @return  void
     */
    public function delete(StoreInterface $store) : void;

    /**
     * @param int $id
     * @return void
     */
    public function deleteById(int $id) : void;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return StoreSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria) : StoreSearchResultInterface;
}
