<?php
declare(strict_types=1);
namespace Elogic\Store\Api\Service;
interface ImportInterface
{
    public function execute();
}
