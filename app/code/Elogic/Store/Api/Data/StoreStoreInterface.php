<?php
declare(strict_types=1);
namespace Elogic\Store\Api\Data;

interface StoreStoreInterface
{
    const ID = "_id";
    const NAME = "name";
    const DESCRIPTION = "description";
    const ELOGIC_ID = "elogic_id";
    const STORE_ID = "store_id";
    /**
     * @return string|null
     */
    public function getName() : ?string;

    /**
     * @return string|null
     */
    public function getDescription() : ?string;

    /**
     * @return int|null
     */
    public function getStoreId() : ?int;
    /**
     * @return int|null
     */
    public function getElogicId() : ?int;
    /**
     * @param int $id
     * @return StoreStoreInterface|null
     */
    public function setId(int $id) : ?StoreStoreInterface;

    /**
     * @param string $name
     * @return StoreStoreInterface
     */
    public function setName(string $name) : StoreStoreInterface;

    /**
     * @param string $description
     * @return StoreStoreInterface
     */
    public function setDescription(string $description) : StoreStoreInterface;

    /**
     * @param int $id
     * @return StoreStoreInterface
     */
    public function setStoreId(int $id) : StoreStoreInterface;

    /**
     * @param int $id
     * @return StoreStoreInterface
     */
    public function setElogicId(int $id) : StoreStoreInterface;
}
