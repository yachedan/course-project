<?php
declare(strict_types=1);
namespace Elogic\Store\Api\Data;

interface StoreInterface
{
    const ID = "_id";
    const NAME = "name";
    const DESCRIPTION = "description";
    const IMAGE = "image";
    const WORK_SCHEDULE = "work_schedule";
    const ADDRESS = "address";
    const LONGITUDE = "longitude";
    const LATITUDE = "latitude";
    const URL_KEY = "url_key";
    const CREATION_TIME = "creation_time";
    const UPDATE_TIME = "update_time";

    /**
     * @return string|null
     */
    public function getName() : ?string;

    /**
     * @return string|null
     */
    public function getDescription() : ?string;

    /**
     * @return string|null
     */
    public function getImage() : ?string;

    /**
     * @return string|null
     */
    public function getWorkSchedule() : ?string;

    /**
     * @return string|null
     */
    public function getAddress() : ?string;

    /**
     * @return string|null
     */
    public function getUrlKey() : ?string;


    /**
     * @return float|null
     */
    public function getLongitude() : ?float;

    /**
     * @return float|null
     */
    public function getLatitude() : ?float;

    /**
     * @return string|null
     */
    public function getCreationTime() : ?string;

    /**
     * @return string|null
     */
    public function getUpdateTime() : ?string;

    /**
     * @param int $id
     * @return StoreInterface|null
     */
    public function setId(int $id) : ?StoreInterface;

    /**
     * @param string $name
     * @return StoreInterface
     */
    public function setName(string $name) : StoreInterface;

    /**
     * @param string $description
     * @return StoreInterface
     */
    public function setDescription(string $description) : StoreInterface;

    /**
     * @param string $image
     * @return StoreInterface
     */
    public function setImage(string $image) : StoreInterface;

    /**
     * @param string $workSchedule
     * @return StoreInterface
     */
    public function setWorkSchedule(string $workSchedule) : StoreInterface;

    /**
     * @param string $address
     * @return StoreInterface
     */
    public function setAddress(string $address) : StoreInterface;

    /**
     * @param string $urlKey
     * @return StoreInterface
     */
    public function setUrlKey(string $urlKey) : StoreInterface;

    /**
     * @param float $longitude
     * @return StoreInterface
     */
    public function setLongitude(float $longitude) : StoreInterface;

    /**
     * @param float $latitude
     * @return StoreInterface
     */
    public function setLatitude(float $latitude) : StoreInterface;

    /**
     * @param string $creationTime
     * @return StoreInterface
     */
    public function setCreationTime(string $creationTime) : StoreInterface;

    /**
     * @param string $updateTime
     * @return StoreInterface
     */
    public function setUpdateTime(string $updateTime) : StoreInterface;
}
