<?php

namespace Elogic\Store\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface StoreSearchResultInterface extends SearchResultsInterface
{

    /**
     * @return \Elogic\Store\Api\Data\StoreInterface[]
     */
    public function getItems();

    /**
     * @param \Elogic\Store\Api\Data\StoreInterface[] $items
     * @return void;
     */
    public function setItems(array  $items);
}
