<?php
declare(strict_types = 1);
namespace Elogic\Store\Service;

use Elogic\Store\Api\Service\ImportInterface;
use Elogic\Store\Model\StoreFactory;
use Elogic\Store\Model\StoreRepository;
use Exception;
use Psr\Log\LoggerInterface;

class GenericCSVImport implements ImportInterface
{
    /**
     * @var string|null
     */
    private $fileName;
    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * logger
     *
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var StoreRepository
     */
    private $repository;

    /**
     * GenericCSVImport constructor.
     * @param string|null $fileName
     */
    public function __construct(string $fileName, StoreFactory $storeFactory, LoggerInterface $logger, StoreRepository $repository)
    {
        $this->fileName = $fileName;
        $this->storeFactory = $storeFactory;
        $this->logger = $logger;
        $this->repository = $repository;
    }

    public function execute()
    {
        $file = fopen('var/import/' . $this->fileName . '.csv', 'r'); // set path to the CSV file
        if ($file !== false) {
            $header = fgetcsv($file);
            $required_data_fields = 1;
            //$this->logger->info(implode($header));
            while ($row = fgetcsv($file)) {
                $row = fgetcsv($file);
                $data_count = count($row);
                if ($data_count < 1) {
                    continue;
                }
                $store = $this->storeFactory->create();
                $data = [];
                $data = array_combine($header, $row);

                $id = $data['id'];
                $name = $data['name'];
                if ($data_count < $required_data_fields || !$name) {
                    $this->logger->info("Skipping store id " . $id . ", not all required fields are present to create the store.");
                    continue;
                }
                $description = $data['short_description'];
                $address = $data['address'] . ',' . $data['city'] . ' ' . $data['country'] . ',' . $data['zip'];
                $workSchedule = $data['schedule'];
                try {
                    $store->setName($name)
                        ->setDescription($description)
                        ->setAddress($address)
                        ->setWorkSchedule($workSchedule);
                    $this->repository->save($store);
                } catch (Exception $e) {
                    $this->logger->info('Error importing product sku: ' . $id . '. ' . $e->getMessage());
                    continue;
                }
            }
            fclose($file);
            return true;
        }
        return false;
    }
}
