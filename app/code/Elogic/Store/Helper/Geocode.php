<?php
declare(strict_types=1);
namespace Elogic\Store\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Geocode extends AbstractHelper
{
    const GOOGLE_MAP_CONFIG_PATH = 'elogic_store/general/api';

    /**
     * Geocode constructor.
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @return string
     */
    public function getGoogleMapsApiKey(): string
    {
        return $this->scopeConfig->getValue(self::GOOGLE_MAP_CONFIG_PATH);
    }


}
