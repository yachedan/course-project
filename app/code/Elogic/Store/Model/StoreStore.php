<?php
declare(strict_types=1);
namespace Elogic\Store\Model;


use Elogic\Store\Api\Data\StoreStoreInterface;
use Elogic\Store\Model\ResourceModel\StoreStore as StoreResource;
use Magento\Framework\Model\AbstractModel;

class StoreStore extends AbstractModel implements StoreStoreInterface
{
    protected $_eventPrefix = "store_store";

    protected $_eventObject = "store_store";
    public function _construct()
    {
        $this->_init(StoreResource::class);
    }
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->getData(self::NAME);
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @return int|null
     */
    public function getStoreId(): ?int
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * @return int|null
     */
    public function getElogicId(): ?int
    {
        return $this->getData(self::ELOGIC_ID);
    }
    /**
     * @param  $value
     * @return StoreStoreInterface
     */
    public function setId($value): StoreStoreInterface
    {
        return $this->setData(self::ID, $value);
    }

    /**
     * @param string $name
     * @return StoreStoreInterface
     */
    public function setName(string $name): StoreStoreInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @param string $description
     * @return StoreStoreInterface
     */
    public function setDescription(string $description): StoreStoreInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @param int $id
     * @return StoreStoreInterface
     */
    public function setStoreId(int $id): StoreStoreInterface
    {
        return $this->setData(self::STORE_ID, $id);
    }
    /**
     * @param int $id
     * @return StoreStoreInterface
     */
    public function setElogicId(int $id): StoreStoreInterface
    {
        return $this->setData(self::ELOGIC_ID, $id);
    }
}
