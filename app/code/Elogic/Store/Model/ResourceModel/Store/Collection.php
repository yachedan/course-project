<?php
namespace Elogic\Store\Model\ResourceModel\Store;

use Elogic\Store\Model\ResourceModel\Store as StoreResource;
use Elogic\Store\Model\Store;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Store::class, StoreResource::class);
    }
}
