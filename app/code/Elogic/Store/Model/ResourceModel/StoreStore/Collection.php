<?php
namespace Elogic\Store\Model\ResourceModel\StoreStore;

use Elogic\Store\Model\ResourceModel\StoreStore as StoreResource;
use Elogic\Store\Model\StoreStore;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(StoreStore::class, StoreResource::class);
    }
}
