<?php
declare(strict_types=1);
namespace Elogic\Store\Model;

use Elogic\Store\Api\Data\StoreInterface;
use Elogic\Store\Api\Data\StoreSearchResultInterface;
use Elogic\Store\Api\Data\StoreSearchResultInterfaceFactory;
use Elogic\Store\Api\StoreRepositoryInterface;
use Elogic\Store\Model\ResourceModel\Store\Collection;
use Elogic\Store\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;
use Elogic\Store\Model\ResourceModel\StoreFactory as StoreResourceFactory;
use Elogic\Store\Model\StoreFactory as StoreFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Exception\NoSuchEntityException;

class StoreRepository implements StoreRepositoryInterface
{
    /**
     * @var StoreResourceFactory
     */
    private $storeResourceFactory;
    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * @var StoreCollectionFactory
     */
    private $storeCollectionFactory;

    /**
     * @var StoreSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var EventManager
     */
    private $_eventManager;

    /**
     * StoreRepository constructor.
     * @param StoreFactory $storeFactory
     * @param StoreResourceFactory $storeResourceFactory
     * @param StoreCollectionFactory $storeCollectionFactory
     * @param StoreSearchResultInterfaceFactory $storeSearchResultInterfaceFactory
     * @param EventManager $_eventManager
     */
    public function __construct(
        StoreFactory $storeFactory,
        StoreResourceFactory $storeResourceFactory,
        StoreCollectionFactory $storeCollectionFactory,
        StoreSearchResultInterfaceFactory $storeSearchResultInterfaceFactory,
        EventManager $_eventManager
    ) {
        $this->storeFactory = $storeFactory;
        $this->storeResourceFactory = $storeResourceFactory;
        $this->storeCollectionFactory = $storeCollectionFactory;
        $this->searchResultFactory = $storeSearchResultInterfaceFactory;
        $this->_eventManager = $_eventManager;
    }

    /**
     * @param int $id
     * @return Store
     * @throws NoSuchEntityException
     */
    public function getById(int $id) : StoreInterface
    {
        $store = $this->storeFactory->create();
        $this->storeResourceFactory->create()->load($store, $id);
        if (!$store->getId()) {
            throw new NoSuchEntityException(__('There is no Store with this ID'));
        }
        return $store;
    }

    /**
     * @param string $urlKey
     * @return Store
     * @throws NoSuchEntityException
     */
    public function getByUrlKey(string $urlKey) : StoreInterface
    {
        $store = $this->storeFactory->create();
        $this->storeResourceFactory->create()->load($store, $urlKey, 'url_key');
        if (!$store->getId()) {
            throw new NoSuchEntityException(__('There is no Store with this URL'));
        }
        return $store;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return StoreSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria) : StoreSearchResultInterface
    {
        $collection = $this->storeCollectionFactory->create();

        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     */
    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $field[] = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }
    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ((array) $searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() == SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }
    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }
    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, Collection $collection): StoreSearchResultInterface
    {
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
    /**
     * @param StoreInterface $store
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @return  void
     */
    public function save(StoreInterface $store): void
    {
        $this->_eventManager->dispatch('elogic_store_save_before', ['store' => $store]);
        $this->storeResourceFactory->create()->save($store);
    }

    /**
     * @param StoreInterface $store
     * @throws \Exception
     * @return void
     */
    public function delete(StoreInterface $store) : void
    {
        $this->storeResourceFactory->create()->delete($store);
    }

    /**
     * @param int $id
     * @throws NoSuchEntityException
     * @throws \Exception
     * @return void
     */
    public function deleteById(int $id) : void
    {
        $store = $this->getById($id);
        $this->delete($store);
    }
}
