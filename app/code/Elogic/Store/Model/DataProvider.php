<?php

declare(strict_types=1);

namespace Elogic\Store\Model;

use Elogic\Store\Api\StoreRepositoryInterface;
use Elogic\Store\Api\StoreStoreRepositoryInterface;
use Elogic\Store\Model\ResourceModel\Store\CollectionFactory;
use Magento\Framework\App\RequestInterface;
use Elogic\Store\Model\StoreFactory;
use Elogic\Store\Model\StoreStoreFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;
use Magento\Store\Model\StoreManagerInterface;

class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;
    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var array
     */
    protected $loadedData;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var StoreStoreRepositoryInterface
     */
    private $storeStoreRepository;
    /**
     * @var \Elogic\Store\Model\StoreStoreFactory
     */
    private $storeStoreFactory;


    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     * @param null $pool
     * @param RequestInterface $request
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreStoreRepositoryInterface $storeStoreRepository
     * @param \Elogic\Store\Model\StoreStoreFactory $storeStoreFactory
     * @param StoreFactory $storeFactory
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = [],
        $pool = null,
        RequestInterface $request,
        StoreRepositoryInterface $storeRepository,
        StoreStoreRepositoryInterface $storeStoreRepository,
        StoreStoreFactory $storeStoreFactory,
        StoreFactory $storeFactory,
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->request = $request;
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeFactory;
        $this->collectionFactory = $collectionFactory;
        $this->collection = $collectionFactory->create();
        $this->storeManager = $storeManager;
        $this->storeStoreRepository = $storeStoreRepository;
        $this->storeStoreFactory = $storeStoreFactory;
    }

    /**
     * @return array
     * @throws NoSuchEntityException
     */
    public function getData(): array
    {
        $id = $this->request->getParam($this->requestFieldName);
        try {
            $store = $this->storeRepository->getById((int)$id);
        } catch (NoSuchEntityException $exception) {
            $store = $this->storeFactory->create();
        }
        $this->loadedData[$store->getId()] = $store->getData();
        $items = $this->collection->getItems();

        //Replace icon with fileuploader field name
        foreach ($items as $model) {
            $model->setStoreId($this->request->getParam('store'));
            $this->loadedData[$model->getId()] = $model->getData();
            if ($model->getImage()) {
                $m['image'][0]['name'] = $model->getImage();
                $m['image'][0]['url'] = $this->getMediaUrl() . $model->getImage();
                $fullData = $this->loadedData;
                $this->loadedData[$model->getId()] = array_merge($fullData[$model->getId()], $m);
            }
        }
        $storeId = $this->request->getParam('store');
        if ($storeId) {
            $storeStore = $this->storeStoreRepository->getByIds((int)$id, (int)$storeId);
            $fullData = $this->loadedData;
            $this->loadedData[$store->getId()] = array_merge($fullData[$model->getId()], ['name'=>$storeStore->getName(),
                'description'=>$storeStore->getDescription()]);
        }
        return $this->loadedData;
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    public function getMediaUrl(): string
    {
        $mediaUrl = $this->storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'elogic_store/tmp/image/';
        return $mediaUrl;
    }
}
