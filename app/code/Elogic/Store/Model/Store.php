<?php
declare(strict_types=1);
namespace Elogic\Store\Model;

use Elogic\Store\Api\Data\StoreInterface;
use Elogic\Store\Model\ResourceModel\Store as StoreResource;
use Magento\Framework\Model\AbstractModel;

class Store extends AbstractModel implements StoreInterface
{
    protected $_eventPrefix = "store";

    protected $_eventObject = "store";
    public function _construct()
    {
        $this->_init(StoreResource::class);
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->getData(self::NAME);
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * @return string|null
     */
    public function getWorkSchedule(): ?string
    {
        return $this->getData(self::WORK_SCHEDULE);
    }

    /**
     * @return string|null
     */
    public function getUrlKey(): ?string
    {
        return $this->getData(self::URL_KEY);
    }


    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return (float) $this->getData(self::LONGITUDE);
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return (float) $this->getData(self::LATITUDE);
    }

    /**
     * @return string|null
     */
    public function getCreationTime(): ?string
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @return string|null
     */
    public function getUpdateTime(): ?string
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @param mixed $value
     * @return StoreInterface
     */
    public function setId($value): StoreInterface
    {
        return $this->setData(self::ID, $value);
    }

    /**
     * @param string $name
     * @return StoreInterface
     */
    public function setName(string $name): StoreInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @param string $description
     * @return StoreInterface
     */
    public function setDescription(string $description): StoreInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @param string $image
     * @return StoreInterface
     */
    public function setImage(string $image): StoreInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * @param string $address
     * @return StoreInterface
     */
    public function setAddress(string $address): StoreInterface
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * @param string $urlKey
     * @return StoreInterface
     */
    public function setUrlKey(string $urlKey): StoreInterface
    {
        return $this->setData(self::URL_KEY, $urlKey);
    }

    /**
     * @param string $workSchedule
     * @return StoreInterface
     */
    public function setWorkSchedule(string $workSchedule): StoreInterface
    {
        return $this->setData(self::WORK_SCHEDULE, $workSchedule);
    }

    /**
     * @param float $longitude
     * @return StoreInterface
     */
    public function setLongitude(float $longitude): StoreInterface
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * @param float $latitude
     * @return StoreInterface
     */
    public function setLatitude(float $latitude): StoreInterface
    {
        return $this->setData(self::LATITUDE, $latitude);
    }

    /**
     * @param string $creationTime
     * @return StoreInterface
     */
    public function setCreationTime(string $creationTime) : StoreInterface
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * @param string $updateTime
     * @return StoreInterface
     */
    public function setUpdateTime(string $updateTime): StoreInterface
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }
}
