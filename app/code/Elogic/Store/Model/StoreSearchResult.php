<?php

namespace Elogic\Store\Model;

use Elogic\Store\Api\Data\StoreSearchResultInterface;
use Magento\Framework\Api\SearchResults;

class StoreSearchResult extends SearchResults implements StoreSearchResultInterface
{
}
