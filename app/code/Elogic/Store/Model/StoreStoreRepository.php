<?php
declare(strict_types=1);
namespace Elogic\Store\Model;

use Elogic\Store\Api\Data\StoreStoreInterface;
use Elogic\Store\Api\StoreStoreRepositoryInterface;
use Elogic\Store\Model\ResourceModel\StoreStore\CollectionFactory;
use Elogic\Store\Model\ResourceModel\StoreStoreFactory as StoreStoreResourceFactory;

class StoreStoreRepository implements StoreStoreRepositoryInterface
{
    /**
     * @var StoreStoreResourceFactory
     */
    private $storeStoreResourceFactory;
    /**
     * @var StoreStoreFactory
     */
    private $storeStoreFactory;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * StoreStoreRepository constructor.
     * @param \Elogic\Store\Model\StoreStoreFactory $storeStoreFactory
     * @param StoreStoreResourceFactory $storeStoreResourceFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        StoreStoreFactory $storeStoreFactory,
        StoreStoreResourceFactory $storeStoreResourceFactory,
        CollectionFactory $collectionFactory
    ) {
        $this->storeStoreFactory = $storeStoreFactory;
        $this->storeStoreResourceFactory = $storeStoreResourceFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param int $id
     * @return StoreStoreInterface
     */
    public function getById(int $id) : StoreStoreInterface
    {
        $store = $this->storeStoreFactory->create();
        $this->storeStoreResourceFactory->create()->load($store, $id);
        return $store;
    }

    /**
     * @param int $id
     * @param int $storeId
     * @return StoreStoreInterface
     */
    public function getByIds(int $id, int $storeId) : StoreStoreInterface
    {
        $store = $this->storeStoreFactory->create();
        $collection = $this->collectionFactory->create();
        $collection =  $collection->addFieldToFilter('store_id', $storeId)->addFieldToFilter('elogic_id', $id)->load()->getFirstItem();
        return $collection;
    }

    /**
     * @param StoreStoreInterface $store
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(StoreStoreInterface $store): void
    {
        $this->storeStoreResourceFactory->create()->save($store);
    }

    /**
     * @param StoreStoreInterface $store
     * @throws \Exception
     */
    public function delete(StoreStoreInterface $store) : void
    {
        $this->storeStoreResourceFactory->create()->delete($store);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function deleteById(int $id) : void
    {
        $store = $this->getById($id);
        $this->delete($store);
    }
}
