<?php

namespace Elogic\Store\Controller\Adminhtml\Index;

use Elogic\Store\Api\StoreRepositoryInterface;
use Elogic\Store\Api\StoreStoreRepositoryInterface;
use Elogic\Store\Model\StoreFactory;
use Elogic\Store\Model\StoreStoreFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;

class Save extends Action implements HttpGetActionInterface, HttpPostActionInterface
{
    /**
     * Storeization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Elogic_Store::save';
    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;
    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var RedirectFactory
     */
    private $redirectFactory;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var StoreStoreRepositoryInterface
     */
    private $storeStoreRepository;
    /**
     * @var StoreStoreFactory
     */
    private $storeStoreFactory;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct(
        Context $context,
        StoreRepositoryInterface $storeRepository,
        StoreStoreRepositoryInterface $storeStoreRepository,
        StoreFactory $storeFactory,
        StoreStoreFactory $storeStoreFactory,
        RedirectFactory $redirectFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->storeRepository = $storeRepository;
        $this->storeFactory = $storeFactory;
        $this->redirectFactory = $redirectFactory;
        $this->storeManager = $storeManager;
        $this->storeStoreRepository = $storeStoreRepository;
        $this->storeStoreFactory = $storeStoreFactory;
    }

    /**
     * execute
     *
     *
     */
    public function execute()
    {
        $storeData = $this->getRequest()->getParams();
        $data = $this->filterData($storeData);

        $store = $this->storeFactory->create();
        $resultRedirect = $this->resultRedirectFactory->create();

        if (empty($data['_id'])) {
            $data['_id'] = null;
        }

        $id = $this->getRequest()->getParam('_id');
        if ($id) {
            try {
                $store = $this->storeRepository->getById($id);
            } catch (LocalizedException $e) {
                return $resultRedirect->setPath('*/*/');
            }
        }
        $storeId = $this->getRequest()->getParam('store_id');
        if ($storeId) {
            $storeStore = $this->storeStoreFactory->create();
            $storeStore->setName($data['name']);
            $storeStore->setDescription($data['description']);
            $storeStore->setElogicId($data['_id']);
            $storeStore->setStoreId($data['store_id']);
            $this->storeStoreRepository->save($storeStore);
            unset($data['name']);
            unset($data['description']);
            $store->setData($data);
        } else {
            $store->setData($data);
        }
        $this->storeRepository->save($store);
        return $resultRedirect->setPath("*/*/index");
    }
    public function filterData(array $rawData)
    {
        $data = $rawData;
        if (isset($data['image'][0]['name'])) {
            $data['image'] = $data['image'][0]['name'];
        } else {
            $data['image'] = null;
        }
        return $data;
    }
}
