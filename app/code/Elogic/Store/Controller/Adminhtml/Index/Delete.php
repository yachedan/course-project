<?php
declare(strict_types=1);
namespace Elogic\Store\Controller\Adminhtml\Index;

use Elogic\Store\Api\StoreRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends Action implements HttpPostActionInterface
{
    /**
     * Storeization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Elogic_Store::store_delete';
    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * Delete constructor.
     * @param Context $context
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        StoreRepositoryInterface $storeRepository
    ) {
        parent::__construct($context);
        $this->storeRepository = $storeRepository;
    }

    public function execute()
    {
        $storeId = $this->getRequest()->getParam('_id');
        if (!$storeId) {
            $this->messageManager->addErrorMessage("No store Specified");
        }

        try {
            $this->storeRepository->deleteById((int)$storeId);
            $this->messageManager->addSuccessMessage("Store Removed");
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage("Can not find store to delete");
        }

        $redirect = $this->resultRedirectFactory->create();

        return $redirect->setPath("*/*/index");
    }
}
