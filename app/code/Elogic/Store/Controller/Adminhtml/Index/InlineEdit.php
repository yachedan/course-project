<?php
declare(strict_types=1);
namespace Elogic\Store\Controller\Adminhtml\Index;

use Elogic\Store\Api\StoreRepositoryInterface as StoreRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * Storeization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Elogic_Store::save';

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;
    /**
     * @var \Elogic\Store\Api\StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * InlineEdit constructor.
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param StoreRepository $storeRepository
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        StoreRepository $storeRepository
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->storeRepository = $storeRepository;
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData(
                [
                    'messages' => [__('Please correct the data sent.')],
                    'error' => true,
                ]
            );
        }
        foreach (array_keys($postItems) as $storeId) {
            /** @var \Elogic\Store\Model\Store $store */
            $store = $this->storeRepository->getById((int)$storeId);
            try {
                $store->setData(array_merge($store->getData(), $postItems[$storeId]));
                $this->storeRepository->save($store);
            } catch (\Exception $e) {
                $messages[] = "[Store ID: {$storeId}]  {$e->getMessage()}";
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
