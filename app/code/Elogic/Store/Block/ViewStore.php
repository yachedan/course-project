<?php
declare(strict_types=1);
namespace Elogic\Store\Block;

use Elogic\Store\Api\Data\StoreInterface;
use Elogic\Store\Api\StoreRepositoryInterface;
use Elogic\Store\Model\StoreRepository;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Element\Template;

class ViewStore extends Template
{
    /**
     * @var StoreRepository
     */
    private $storeRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * ViewStore constructor.
     * @param Template\Context $context
     * @param StoreRepositoryInterface $storeRepository
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        StoreRepositoryInterface $storeRepository,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);
        $this->storeRepository = $storeRepository;
    }
    /**
     * @return StoreInterface|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStore() : ?StoreInterface
    {
        $id = $this->getRequest()->getParam("id");
        return $this->storeRepository->getByUrlKey($id);
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaUrl(): string
    {
        return $this->storeManager->getStore()->getBaseUrl(
            UrlInterface::URL_TYPE_MEDIA
        ) . 'elogic_store/tmp/image/' . $this->getStore()->getImage();
    }
}
