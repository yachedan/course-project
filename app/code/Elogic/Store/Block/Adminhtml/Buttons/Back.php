<?php

namespace Elogic\Store\Block\Adminhtml\Buttons;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Back implements ButtonProviderInterface
{
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * Back constructor.
     * @param UrlInterface $url
     */
    public function __construct(
        UrlInterface $url
    )
    {
        $this->url = $url;
    }

    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->url->getUrl("*/*/index")),
            'class' => 'back',
            'sort_order' => 10
        ];
    }
}
