<?php
declare(strict_types=1);
namespace Elogic\Store\Block;

use Elogic\Store\Model\ResourceModel\Store\CollectionFactory;
use Elogic\Store\Api\StoreRepositoryInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;

class GetAll extends Template
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * ViewStore constructor.
     * @param Template\Context $context
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);

        $this->collectionFactory = $collectionFactory;
    }


    /**
     * @return \Elogic\Store\Model\ResourceModel\Store\Collection
     */
    public function getAll(): \Elogic\Store\Model\ResourceModel\Store\Collection
    {
        $collection = $this->collectionFactory->create()->setPageSize(20)->load();
        return $collection;
    }

    /**
     * @param string $image
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaUrl(string $image): string
    {
        return $this->storeManager->getStore()->getBaseUrl(
            UrlInterface::URL_TYPE_MEDIA
        ) . 'elogic_store/tmp/image/' . $image;
    }
}
