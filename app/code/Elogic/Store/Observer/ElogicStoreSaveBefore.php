<?php
declare(strict_types=1);
namespace Elogic\Store\Observer;

use Elogic\Store\Api\Data\StoreInterface;
use Elogic\Store\Helper\Geocode;
use Elogic\Store\Model\StoreRepository;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ElogicStoreSaveBefore implements ObserverInterface
{
    /**
     * @var Geocode
     */
    private $helper;

    /**
     * @var StoreRepository
     */
    private $repository;

    /**
     * ElogicStoreSaveBefore constructor.
     * @param Geocode $helper
     * @param StoreRepository $repository
     */
    public function __construct(Geocode $helper, StoreRepository $repository)
    {
        $this->helper = $helper;
        $this->repository = $repository;
    }

    /**
     * @param StoreInterface $store
     * @return array
     */
    public function fetchAddressGeoCoordinates(StoreInterface $store): array
    {
        $coordinates = [];
        $address = $store->getAddress();
        $url = "http://mapquestapi.com/geocoding/v1/address?key=" . $this->helper->getGoogleMapsApiKey() . "&location=" . urlencode($address);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $responseJson = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($responseJson);

        if ($response->info->statuscode == 0) {
            $latitude = $response->results[0]->locations[0]->latLng->lat;
            $longitude = $response->results[0]->locations[0]->latLng->lng;

            $store->setLongitude($longitude);
            $store->setLatitude($latitude);
        }
        return $coordinates;
    }

    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer): ElogicStoreSaveBefore
    {
        $this->fetchAddressGeoCoordinates($observer->getEvent()->getStore());
        return $this;
    }
}
